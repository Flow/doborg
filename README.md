doborg - Full-System Backup via Borg as Unprivileged User
=========================================================

This utility allows to perform full-system backups using Borg
backup. Unlike most (all?) other backup utilities, the backup process
is run as *unprivileged user* via the magic of Linux capabilities
(CAP_DAC_READ_SEARCH in particular).

Configuration
-------------

### General Layout

**`$config/repository`**

Contains the URL of the remote Borg repository.
Example:
```
btl55s5u@btl55s5u.repo.borgbase.com:repo
```

**`$config/passphrase`**

Contains the passphrase used by Borg.

**`$config/key`**

Contions the key of remote Borg repository.

**`$config/excludes`**

Custom backup exclude configuration.

**`$config/prune_configuration`**

Custom prune configuration.
