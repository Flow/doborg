DEFAULT: check

SYSTEMD_SYSTEM_UNIT_DIR ?= /lib/systemd/system

PHONY: install
install:
	install -D doborg "${DESTDIR}/usr/bin/doborg"
	install -d "${DESTDIR}/${SYSTEMD_SYSTEM_UNIT_DIR}"
	install --mode=644 doborg.service doborg.timer "${DESTDIR}/${SYSTEMD_SYSTEM_UNIT_DIR}"

PHONY: check
check:
	shellcheck doborg

PHONY: doborg-user
doborg-user:
	useradd --home-dir /var/lib/doborg --create-home --system --expiredate 1970-01-01 --shell /usr/sbin/nologin doborg
	passwd --lock doborg
